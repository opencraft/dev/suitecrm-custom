# /usr/bin/env bash
set -euo pipefail
echo -e "\e[32mCopying SuiteCRM changes from the pod to the custom directory...\e[0m"
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
POD_NAME=$(kubectl get pod -nsuitecrm -o jsonpath="{.items[0].metadata.name}")
kubectl cp suitecrm/${POD_NAME}:/bitnami/suitecrm/custom $SCRIPT_DIR/../custom
