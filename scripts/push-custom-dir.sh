# /usr/bin/env bash
set -euo pipefail
echo -e "\e[33mPushing the custom directory to the SuiteCRM pod...\e[0m"
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
POD_NAME=$(kubectl get pod -nsuitecrm -o jsonpath="{.items[0].metadata.name}")
kubectl cp $SCRIPT_DIR/../custom suitecrm/${POD_NAME}:/bitnami/suitecrm/
