<?php
// created: 2022-12-16 23:14:30
$mod_strings['LBL_STATUS'] = 'Status:';
$mod_strings['LBL_ORGANIZATION'] = 'Organization';
$mod_strings['LBL_ACCOUNT_NAME'] = 'Organization:';
$mod_strings['LBL_POSTMORTEMREASON'] = 'Post-mortem reason';
$mod_strings['LBL_MONDAYID'] = 'Monday Id';
$mod_strings['LBL_TICKET'] = 'Ticket';
$mod_strings['LBL_PRIORITY'] = 'Priority';
$mod_strings['LBL_ORGANIZATION_TYPE'] = 'organization type';
$mod_strings['LBL_DEADLINE'] = 'Deadline';
$mod_strings['LBL_LAST_NAME'] = 'Name';
$mod_strings['LBL_NAME'] = 'Name';
