<?php
 // created: 2022-12-15 19:45:47
$dictionary['Lead']['fields']['account_name']['inline_edit']=true;
$dictionary['Lead']['fields']['account_name']['comments']='Name of organization this lead is from';
$dictionary['Lead']['fields']['account_name']['merge_filter']='disabled';
$dictionary['Lead']['fields']['account_name']['full_text_search']=NULL;

 ?>