<?php
$app_list_strings['cell_list']=array (
  'Bebop' => 'Bebop',
  'Falcon' => 'Falcon',
  'Serenity' => 'Serenity',
  'Deathstar' => 'Deathstar',
  'Meta' => 'Meta',
);

$app_list_strings['contract_type_0']=array (
  'institutional' => 'Institutional / dev',
  'pro_and_teacher' => 'Pro & Teacher Hosting',
  'support' => 'Support',
);
$app_list_strings['priority_list']=array (
  'Low' => 'Low',
  'Mid' => 'Mid',
  'High' => 'High',
);
$app_list_strings['organization_type_list']=array (
  'ForProfit' => 'For-Profit Corperation',
  'NonProfit' => 'Non-Profit Corperation',
  'HigherEducation' => 'Higher Education',
  'PrimaryOrSecondary' => 'Primary or Secondary Education',
  'Government' => 'Government',
);

$app_list_strings['lead_status_dom']=array (
  '' => '',
  'New' => 'New',
  'Qualifying' => 'Prospect Qualification',
  'Assess' => 'Assess Needs',
  'Discovery' => 'In Discovery',
  'Propose' => 'Propose',
  'CheckIn' => 'Periodic Checkin',
  'Paused' => 'Paused',
  'Won' => 'Closed - Won',
  'Closed' => 'Closed - Other',
  'Lost' => 'Closed - Lost',
  'Disqualified' => 'Disqualified',
);
$app_list_strings['postmortemreason_list']=array (
  'BadFit' => 'Bad fit',
  'Budget' => 'Budget',
  'Custom' => 'Building their own custom solution',
  'NonOEX' => 'Chose non-Open edX Solution',
  'OtherOEX' => 'Chose other Open edX Provider',
  'Duplicate' => 'Duplicate',
  'Ghosted' => 'Ghosted',
  'OurOrgFailure' => 'Organizational Failure on our end',
  'TheirOrgFailure' => 'Organizational Failure on their end',
  'Cancelled' => 'Project Cancelled',
  'SelfHosting' => 'Self-hosting',
  'Spam' => 'Spam or Not a Sales Lead',
  'NotProvided' => 'Wants a service we don\'t provide',
  'Capacity' => 'We have no capacity',
  'Deprioritized' => 'deprioritized',
  'edXReject' => 'edX.org Rejection',
);
$app_list_strings['contact_type_list']=array (
  'product_lead' => 'Product Lead',
  'billing' => 'Billing',
  'project_lead' => 'Project Lead',
  'tech_contact' => 'Tech Contact',
  'admin' => 'Admin',
);