<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2022-12-15 16:42:41
$dictionary['Note']['fields']['testfield_c']['labelValue']='testfield';

 

 // created: 2022-12-15 17:33:30
$dictionary['Note']['fields']['note_c']['labelValue']='Note';

 

 // created: 2022-12-15 17:03:57
$dictionary['Note']['fields']['description']['inline_edit']=true;
$dictionary['Note']['fields']['description']['comments']='Full text of the note';
$dictionary['Note']['fields']['description']['merge_filter']='disabled';

 
?>