<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2022-12-15 02:42:40

 

 // created: 2022-12-16 23:37:16
$dictionary['Lead']['fields']['last_name']['comments']='This is \'last name\' in the system, but we override it to be the entire name, since name structures vary too much to ensure a \'last name\'.';

 

 // created: 2022-12-15 21:00:46
$dictionary['Lead']['fields']['postmortemreason_c']['inline_edit']='1';
$dictionary['Lead']['fields']['postmortemreason_c']['labelValue']='Post-mortem reason';

 

 // created: 2022-12-15 19:39:57
$dictionary['Lead']['fields']['organization_c']['inline_edit']='1';
$dictionary['Lead']['fields']['organization_c']['labelValue']='Organization';

 

 // created: 2022-12-05 14:51:45
$dictionary['Lead']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2022-12-05 14:51:45
$dictionary['Lead']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2022-12-05 14:51:45
$dictionary['Lead']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 

 // created: 2022-12-15 19:45:47
$dictionary['Lead']['fields']['account_name']['inline_edit']=true;
$dictionary['Lead']['fields']['account_name']['comments']='Name of organization this lead is from';
$dictionary['Lead']['fields']['account_name']['merge_filter']='disabled';
$dictionary['Lead']['fields']['account_name']['full_text_search']=NULL;

 

 // created: 2022-12-15 23:43:59
$dictionary['Lead']['fields']['organization_type_c']['inline_edit']='1';
$dictionary['Lead']['fields']['organization_type_c']['labelValue']='organization type';

 

 // created: 2022-12-15 23:08:29
$dictionary['Lead']['fields']['ticket_c']['inline_edit']='1';
$dictionary['Lead']['fields']['ticket_c']['labelValue']='Ticket';

 

 // created: 2022-12-16 23:08:47
$dictionary['Lead']['fields']['name_c']['inline_edit']='1';
$dictionary['Lead']['fields']['name_c']['labelValue']='Name';

 

 // created: 2022-12-16 00:23:49
$dictionary['Lead']['fields']['mondayid_c']['inline_edit']='1';
$dictionary['Lead']['fields']['mondayid_c']['labelValue']='Monday Id';

 

 // created: 2022-12-16 22:06:56
$dictionary['Lead']['fields']['deadline_c']['inline_edit']='1';
$dictionary['Lead']['fields']['deadline_c']['labelValue']='Deadline';

 

 // created: 2022-12-05 14:51:45
$dictionary['Lead']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

 // created: 2022-12-15 23:10:28
$dictionary['Lead']['fields']['priority_c']['inline_edit']='1';
$dictionary['Lead']['fields']['priority_c']['labelValue']='Priority';

 
?>