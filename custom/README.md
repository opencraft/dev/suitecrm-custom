# Suitecrm Custom Files

This repo contains file overrides for our SuiteCRM instance.
Read [this article](https://pgorod.github.io/Custom-folder/) to understand how this works.

## Deployment

1. Setup the [infrastructure](https://gitlab.com/opencraft/ops/infrastructure) repository.
1. Get suitecrm pod id: `kubectl get pods -n suitecrm`.
1. Connect to the pod: `kubectl exec -it <POD_ID> -n suitecrm -- /bin/bash`.
1. Change directory to `custom`: `cd /bitnami/suitecrm/custom`.
1. Download `suitecrm-custom` repository archive: `curl -L https://gitlab.com/opencraft/dev/suitecrm-custom/-/archive/main/suitecrm-custom-main.tar.gz > suitecrm-custom-main.tar.gz`.
1. Extract the archive:
