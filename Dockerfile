FROM bitnami/suitecrm:7.14.3

# Install supercronic, a cron implementation that can run as a non-root user.

# Latest releases available at https://github.com/aptible/supercronic/releases
ENV SUPERCRONIC_URL=https://github.com/aptible/supercronic/releases/download/v0.2.29/supercronic-linux-amd64 \
    SUPERCRONIC=supercronic-linux-amd64 \
    SUPERCRONIC_SHA1SUM=cd48d45c4b10f3f0bfdd3a57d054cd05ac96812b

RUN curl -fsSLO "$SUPERCRONIC_URL" \
 && echo "${SUPERCRONIC_SHA1SUM}  ${SUPERCRONIC}" | sha1sum -c - \
 && chmod +x "$SUPERCRONIC" \
 && mv "$SUPERCRONIC" "/usr/local/bin/${SUPERCRONIC}" \
 && ln -s "/usr/local/bin/${SUPERCRONIC}" /usr/local/bin/supercronic

# Copy over the custom modules folder
COPY --chown=1001:1001 custom /opt/suitecrm-custom/
# Create the crontab
COPY overrides/crontab /opt/bitnami/crontab
# Copy over the modified run.sh that starts crontab
COPY overrides/run.sh /opt/bitnami/scripts/suitecrm/run.sh

RUN rm -r /opt/bitnami/suitecrm/custom/ && \
    cp -r /opt/suitecrm-custom /opt/bitnami/suitecrm/custom && \
    chown -R 1001:1001 /opt/bitnami/suitecrm/custom

