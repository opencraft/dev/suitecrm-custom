## Suitecrm Custom Files

This repo contains file overrides for our SuiteCRM instance.
Read [this article](https://pgorod.github.io/Custom-folder/) to understand how this works.

## Custom Docker Image

We use a custom Docker image to run SuiteCRM so that:

- Cron jobs can be run. By default SuiteCRM does not install the cronjobs due to a lack of root privileges on Kubernetes. By building our own image, we can add this cronjob as part of the build process.
- Custom code: Any customizations can and should be deployed when new changes are made. Scripts are provided to facilitate this.

The image is automatically built on every push to the repository and tagged by slugifying the branch-name. Images built from change on `main` branch
are tagged `latest`.

## Applying your customizations

- `./scripts/fetch-custom-dir.sh` is a batch script that will fetch any customizations made on your installation and place
  them in the `custom` directory. It is up to the user to then commit those changes.
- `./scripts/push-custom-dir.sh` does the opposite and places the local `custom` directory onto the Kubernetes cluster.
